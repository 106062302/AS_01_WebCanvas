# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

#Basic components
Basic control tools (30%)
-Brush and eraser :
    Pencil按鈕就是brush，點擊後可以直接使用。
    Eraser按鈕就是eraser，點擊後可以直接使用。
-Color selector :
    左下角的顏色方塊點擊後就能改變要使用的顏色。
-Simple menu (brush size) :
    size0, size1, size2, size3, size4 由小到大，點擊後就能改變粗度。 

Text input (10%)
-User can type texts on canvas :
    none
-Font menu (typeface and size) :
    none

Cursor icon (10%)
-The image should change according to the currently used tool :
    點擊 line, triangle, Rectangle, circle, Eraser會出現我設定的符標。其中Eraser是使用圖案。點擊其他的畫圖功能還有undo, redo, reset會自動回復成default符標。

Refresh button (10%)
-Reset canvas  :
    點擊Reset按鈕就能回到原始空白頁面。

#Advance tools
Different brush shapes (15%)
Circle, rectangle and triangle (5% for each shape)
-Circle :
    點擊circle按鈕後，按住拖曳後放開就能畫圓。
-rectangle :
    點擊Rectangle按鈕後，按住拖曳後放開就能畫矩形。
-triangle :
    點擊triangle按鈕後，按住拖曳後放開就能畫三角形，還能畫到過來的三角形，只須向上拖曳。

Un/Re-do button (10%) 
-Undo :
    點擊Undo按鈕。
-Redo :
    點擊Redo按鈕。

Image tool (5%)
-User can upload image and paste it :
    點擊選擇檔案按鈕，就能叫出資料庫，選擇圖片並將圖片貼到畫面上。

Download (5%)
-Download current canvas as an image file :
    點擊藍色的Download就能下載並儲存。

